#!/bin/bash

function getDeviceID() {
    name=$@
    echo $(xinput | grep -i "$name" | grep -o 'id=[0-9]*' | cut -d '=' -f 2)
}

function getStatus() {
    echo $(xinput --list-props "$1" | grep -i 'Device Enabled' | cut -f 3)
}

devices=('TouchPad' 'AT Translated Set 2 keyboard')

for i in ${!devices[*]}; do
    id=$(getDeviceID ${devices[$i]})
    status=$(getStatus $id)
    echo -n "ID: $id Name: ${devices[$i]} "
    
    operation="--disable"
    if [[ $status == "0" ]]; then
        operation="--enable"
    fi

    echo $operation

    xinput $operation $id
done
